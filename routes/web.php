<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\PostController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', [PostController::class, 'welcome']);

//route for viewing the create posts page
Route::get('/posts/create', [PostController::class, 'create']);
//route for sending form data via POST request to the /posts endpoint
Route::post('/posts', [PostController::class, 'store']);
//route to return a view containing all posts
Route::get('/posts', [PostController::class, 'index']);
//route to tretun a view containing only the authenticated user's posts
Route::get('/posts/myPosts', [PostController::class, 'myPosts']);
//route to return a view showing a specific post's details
Route::get('/posts/{id}', [PostController::class, 'show']);

Route::get('/posts/{id}/edit', [PostController::class, 'edit']);

Route::put('/posts/{id}', [PostController::class, 'update']);

Auth::routes();

Route::get('/home', [App\Http\Controllers\HomeController::class, 'index'])->name('home');

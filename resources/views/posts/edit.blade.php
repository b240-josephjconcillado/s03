@extends('layouts.app');
@section('content')
 <form method="POST" action="/posts/{{$post->id}}">
    @method('PUT')
    @csrf
    <div class="form-group">
        <label for="title">Title: </label>
        <input type="text" class="form-control" id="title" name="title" value="{{$post->title}}">
    </div>
    <div class="form-group">
        <label for="content">Content: </label>
        <textarea class="form-control" name="content" id="content" cols="30" rows="3">{{$post->content}}</textarea>
    </div>
    <div class="mt-2">
        <button type="submit" class="btn btn-primary">
            Update Post
        </button>
    </div>
    <div class="mt-3">
        <a href="/posts" class="card-link">View all posts</a>
        <a href="/posts/myPosts" class="card-link">View my posts</a>
    </div>
 </form>
 @endsection